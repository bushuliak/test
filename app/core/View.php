<?php

namespace app\core;

class View {

    public $route;
    public $path;
    public $layout = 'default';

    public function __construct($route) {
        $this->route = $route;
        $this->path = "{$route['controller']}/{$route['action']}";
    }

    public function render($title, $vars = []) {
        extract($vars);
        if (file_exists($path = "../app/views/{$this->path}.php")) {
            ob_start();
            require $path;
            $content = ob_get_clean();
            if (file_exists($layout = "../app/views/layouts/{$this->layout}.php")) {
                require $layout;
            } else {
                // var_dump($layout);
                self::errorCode(404);
            }
        } else {
            // var_dump($path);
            self::errorCode(404);
        }
    }

    public function redirect($url) {
        header("location: {$url}");
        exit;
    }

    public static function errorCode($code) {
        http_response_code($code);
        if (file_exists($errorPage = "../app/views/errors/{$code}.php")) {
            require $errorPage;
            exit;
        } else {
            throw new Exception('Произошла ошибка. Обновите, пожалуйста, страницу и попробуйте заново.');
        }
    }

    public function message($status, $message) {
        exit(json_encode([ 'status' => $status, 'message' => $message ]));
    }

    public function location($url) {
        exit(json_encode([ 'url' => $url ]));
    }

}
