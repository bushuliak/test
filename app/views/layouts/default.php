<!DOCTYPE html>
<html lang="ru">
    <head>
        <meta charset="utf-8">
        <title><?= $title ?></title>
        <link rel="stylesheet" href="../css/bootstrap.css">
        <link rel="stylesheet" href="../css/style.css">
    </head>
    <body>

        <div class="container">
            <?= $content ?>
        </div>

        <script src="../js/jquery.js"></script>
        <script src="../js/script.js"></script>
    </body>
</html>
