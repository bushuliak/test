<div class="row">
    <div class="col-xs-12 col-sm-10 col-md-8 col-lg-6 col-xl-6 center">
        <div class="card p10">
            <form class="form-signin" action="" method="post">
                <h1>Авторизация</h1>
                <div class="form-group">
                    <label>Email</label>
                    <input type="email" class="form-control" name="email" placeholder="Введите почту" required>
                </div>
                <div class="form-group">
                    <label>Пароль</label>
                    <input type="password" class="form-control" name="password" placeholder="Ведите пароль" required>
                </div>
                <input type="submit" class="btn btn-success" name="login" value="Авторизоваться">
                <a href="/account/register" class="btn btn-primary">Регистрация</a>
            </form>
        </div>
    </div>
</div>
