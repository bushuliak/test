<?php

namespace app\lib;

class Db {

    protected $db = null;

    public function __construct() {
        $config = require __DIR__ . '/../config/db.php';
        $this->db = new \PDO("mysql:host={$config['host']};dbname={$config['dbname']};charset=UTF8",
            $config['user'], $config['password']);
    }

    public function __destruct() {
        // $this->db->close();
        $this->db = null;
    }

    public function query($sql, $params = []) {
        $stmt = $this->db->prepare($sql);
        if (!empty($params)) {
            foreach ($params as $key => $val) $stmt->bindValue(":{$key}", $val);
        }
        $stmt->execute();
        return $stmt;
    }

    public function row($sql, $params = []) {
        $result = $this->query($sql, $params);
        return $result->fetchAll(\PDO::FETCH_ASSOC);
    }

    public function column($sql, $params = []) {
        $result = $this->query($sql, $params);
        return $result->fetchColumn();
    }
    
}
