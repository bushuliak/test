<?php

class Query
{
    private $host = 'localhost';
    private $user = 'root';
    private $pass = 'root';
    private $db = 'bushulyak';

    private $conn = null;

    public function __construct()
    {
        try {
            $this->conn = new PDO("mysql:host=$this->host;dbname=$this->db", $this->user, $this->pass);
            $this->conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        } catch (PDOException $e) {
            file_put_contents(getcwd() . '/error.log', print_r($e->getMessage(), true) . str_repeat(PHP_EOL, 2), FILE_APPEND);
        }
    }

    public function __destruct()
    {
        $this->conn = null;
    }

    public function insert(string $table, array $data): ?array
    {
        try {
            $keysArr = array_keys($data);
            $keysStr = implode(',', $keysArr);

            $valuesArr = array_values($data);
            $valuesStr = implode(',', $valuesArr);

            $placeholders = str_repeat('?,', count($keysArr) - 1) . '?';

            $stmt = $this->conn->prepare("INSERT INTO $table ($keysStr) VALUES ($placeholders)");
            $ok = $stmt->execute($valuesArr);

            // ob_start();
            // $stmt->debugDumpParams();
            // $sql = ob_get_contents();
            // $sql = explode("\n", $sql)[1];
            // ob_end_clean();

            $sql = "INSERT INTO $table ($keysStr) VALUES ($valuesStr)";

            return [
                'ok' => $ok,
                'sql' => $sql
            ];

        } catch (Exception $e) {
            file_put_contents(getcwd() . '/error.log', print_r($e->getMessage(), true) . str_repeat(PHP_EOL, 2), FILE_APPEND);
            return [
                'ok' => false,
                'error' => $e->getMessage()
            ];
        }

    }

    public function select(string $table, array $data = []): ?array
    {
        try {
            $conditions = '';

            if (! empty($data)) {
                $conditions = array_map(function($key, $value) {
                    $value = implode(',', $value);
                    return "$key in ($value)";
                }, array_keys($data), $data);

                $conditions = 'WHERE ' . implode(' AND ', $conditions);
            }

            $stmt = $this->conn->prepare("SELECT * FROM $table $conditions");
            $stmt->execute();

            $stmt->setFetchMode(PDO::FETCH_ASSOC);

            return [
                'ok' => true,
                'data' => $stmt->fetchAll()
            ];

        } catch (Exception $e) {
            file_put_contents(getcwd() . '/error.log', print_r($e->getMessage(), true) . str_repeat(PHP_EOL, 2), FILE_APPEND);
            return [
                'ok' => false,
                'error' => $e->getMessage()
            ];
        }
    }

}

$data = '[{"id":3843,"vendor_code":"2623-39","name":"\u0421\u043b\u0438\u043f\u044b \u043d\u0430\u0442\u0443\u0440\u0430\u043b\u044c\u043d\u0430\u044f \u043a\u043e\u0436\u0430 \u0434\u0435\u043a\u043e\u0440 \u043f\u0447\u0435\u043b\u0430 \ud83d\udc1d .","description":"empty","price":710,"price_wholesale":630,"size":"39","color":"","quantity":1,"available":1,"image":"/filemanager/photos/1/\u041e\u0431\u0443\u0432\u044c/\u041e\u0431\u0443\u0432\u044c 2\ufe0f\u20e3/627DE941-514F-4F37-99EF-DE8B32B338C3.jpeg","preorder":0,"type_id":1,"created_at":"2019-09-30 22:46:02","updated_at":"2019-10-08 16:29:38","image_thumb":"/filemanager/photos/1/\u041e\u0431\u0443\u0432\u044c/\u041e\u0431\u0443\u0432\u044c 2\ufe0f\u20e3/thumbs/627DE941-514F-4F37-99EF-DE8B32B338C3.jpeg"}]';

$data = json_decode($data);
$data = $data[0];
$data = (array) $data;
$data = $data['name'];

$query = new Query();

// $insert = $query->insert('test', [
//     'data' => $data,
// ]);
//
// var_dump($insert);

$select = $query->select('test', [
    'id' => [1, 2]
]);

var_dump($select);
